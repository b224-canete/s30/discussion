// [SECTION] MongoDB Aggregation Method
// Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data

db.fruits.insertMany([
	{
		"name": "Apple",
		"color": "Red",
		"stock": 20,
		"price": 40,
		"supplier_id": 1,
		"onSale": true,
		"origin": ["Philippines","US"]
	},
	{
		"name": "Banana",
		"color": "Yellow",
		"stock": 15,
		"price": 20,
		"supplier_id": 2,
		"onSale": true,
		"origin": ["Philippines","Ecuador"]
	},
	{
		"name": "Kiwi",
		"color": "Green",
		"stock": 25,
		"price": 50,
		"supplier_id": 1,
		"onSale": true,
		"origin": ["US","China"]
	},
	{
		"name": "Mango",
		"color": "Yellow",
		"stock": 10,
		"price": 120,
		"supplier_id": 2,
		"onSale": false,
		"origin": ["Philippines","India"]
	}
	]);

// "$match" - is used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggretation process.
// "$group" - used to group elements together and field-value pairs using the data from the grouped elements.
// "$sum" - operator that calculates and returns the collective sum of numeric values.
/*
SYNTAX
	db.collectionName.aggregate([
	{ $match: { fieldA, valueA } },
	{ $group: { _id: "$fieldB" }, { result: { operation } } }
	])

*/


// Single-Purpose Aggregation Operations
// If we only need simple aggregations with only 1 stage
db.fruits.count();


// Pipelines with multiples stages
db.fruits.aggregate([
	{ $match: {"onSale": true}},
	{ $group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
	]);


// MINI ACTIVITY (5mins)
// Intructions:
// Aggregate the documents in fruits collection using 2 pipeline stages:
// Stage 1: using the $match operator, get all the stock that is gretaer than or equal to 20.
// Stage 2: using the $group operator, group the supplier_id and get the sum of the total number of stocks.

db.fruits.aggregate([
	{ $match: {"stock": {$gte : 20}}},
	{ $group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
	]);


// Field projection with aggregation
// The $project can be used when aggregating data to include/exclude fields from the returned results
/*
SYNTAX:
	{ $project : {field: 1/0}}
*/

db.fruits.aggregate([
	{ $match: {"onSale": true}},
	{ $group: {"_id": "$supplier_id", "total": {$sum: "$stock"}}},
	{ $project: {"_id" : 0}}
	]);


// Sorting aggregating result
// The "$sort" can be used to change the order of aggregated results
// "1" - sort ascending
// "-1" sort descending
db.fruits.aggregate([
	{ $match: {"onSale": true}},
	{ $group: {"_id": "$supplier_id", "total": {$sum: "$stock"}}},
	{ $sort: {"total": 1}}
	]);


// Aggregating results based on array field
db.fruits.aggregate([
	{$unwind: "$origin"}
	]);



// Display fruit documents by their origin and the number of kinds of fruits that they are supllying
db.fruits.aggregate([
	{ $unwind: "$origin"},
	{ $group: {"_id": "$origin", "kinds": {$sum : 1}}}
	]);
